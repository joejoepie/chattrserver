﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Chattr;

namespace ChattrServer
{
    class Program
    {
        Dictionary<string, Socket> clientss;
        BinaryFormatter serializer;
        public static readonly string MOTD = "Welcome to the GoBroStyle Chattr server!";
        void Listen()
        {
            clientss = new Dictionary<string, Socket>();
            serializer = new BinaryFormatter();

            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 6969);

            Socket listener = new Socket(localEndPoint.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            
            try
            {
                listener.Bind(localEndPoint);
                Console.WriteLine("Started listening...");
                listener.Listen(100);

                while (true)
                {
                    Socket client = listener.Accept();
                    Console.WriteLine("Client connected!\tConnected clients: " + (clientss.Count+1));
                    Thread t = new Thread(() => HandleClient(client));
                    t.Start();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private NetworkMessage BytesToMessage(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
            {
                NetworkMessage ret = (NetworkMessage)serializer.Deserialize(ms);
                return ret;
            }
        }
        private void SendMessage(NetworkMessage message, Socket client)
        {
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, message);
                if (ms.Length < 1024)
                {
                    client.Send(ms.ToArray());
                }
            }
        }

        void HandleClient(Socket client)
        {
            byte[] bytes;
            string username = "";
            Task t = Task.Run(() => {
                try
                {
                    bytes = new byte[1024];
                    client.Receive(bytes);
                    NetworkMessage m = BytesToMessage(bytes);
                    if (m.Type == MessageType.Connect)
                        username = m.Username;
                }
                catch(Exception e)
                {
                    Console.WriteLine("Something went wrong with the connection message: " + e.Message);
                }
            });
            t.Wait(1000);
            if (username != "" && !clientss.ContainsKey(username))
            {
                Console.WriteLine("Connection handshake succesful");
                SendConnectedUsersList(client);
                clientss.Add(username, client);
                SendConnectionBroadcast(username, true);

                while (client.Connected)
                {
                    try
                    {
                        bytes = new byte[1024];
                        int bytesRec = client.Receive(bytes);

                        foreach (KeyValuePair<string, Socket> p in clientss)
                        {
                            p.Value.Send(bytes);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                clientss.Remove(username);
                Console.WriteLine("Client disconnected");
                SendConnectionBroadcast(username, false);
            }
            else
            {
                Console.WriteLine("Client didn't connect properly");
            }
        }
        private void SendConnectedUsersList(Socket client)
        {
            NetworkMessage m = new NetworkMessage();
            m.Type = MessageType.Message;
            m.Username = "[SERVER]";
            m.Message = MOTD + "\nConnected clients:\n";
            foreach (KeyValuePair<string, Socket> p in clientss)
                m.Message += "\t" + p.Key + "\n";
            m.Message += '\0';

            try
            {
                SendMessage(m, client);
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to send message: " + e.HResult);
            }
        }
        private void SendConnectionBroadcast(string username, bool connect)
        {
            NetworkMessage m = new NetworkMessage();
            m.Type = MessageType.Message;
            m.Username = "[SERVER]";
            m.Message = "\t[SERVER]: " + username + (connect ? " has connected to the server" : " has disconnected from the server") + '\0';

            foreach (KeyValuePair<string, Socket> p in clientss)
            {
                try
                {
                    SendMessage(m, p.Value);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Failed to send message: " + e.HResult);
                }
            }
        }
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Listen();
        }
    }
}
